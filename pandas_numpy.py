import pandas as pd
import numpy as np
#1 y 2
a = np.array([[1,2],[2,3]])
print(a.shape)

#3
b = np.zeros(shape=(100,100))
#dt2 = pd.DataFrame(b)
print(b)

#4
c = np.identity(5)
print(c)

#5
d = np.array([42,42])
d.fill(42)
#print(d)
#6
e = np.random.rand(3,3)
print(e)
#7
f = np.arange(20).reshape(5, 4)
print(f)
#7.1
g = f[[2,2,2,3,3,3,4,4,4],[0,1,2,0,1,2,0,1,2]].reshape(3,3)
print(g)


#Pandas section
#1
diccionario2 = {
1:"Santos Laguna"
,2:"León"
,3:"Tigres"
,4:"Querétaro"
,5:"Necaxa"
,6:"América"
,7:"Monarcas Morelia"
,8:"Monterrey"
,9:"Pachuca"
,10:"Chivas"
,11:"Club Tijuana"
,12:"Cruz Azul"
,13:"Pumas UNAM"
,14:"Atlas"
,15:"Atlético San Luis"
,16:"FC Juárez"
,17:"Toluca"
,18:"Puebla"
,19:"Veracruz",}
#diccionario = {"Product":{"0":"Desktop Computer","1":"Tablet","2":"iPhone","3":"Laptop"},"Price":{"0":700,"1":250,"2":800,"3":1200}}
#pd.read_json(diccionario2)

data1 = pd.read_json(r'/Users/ramiroperez/Library/Mobile Documents/com~apple~CloudDocs/MCD/progra2/json_test.json')

#3
print(data1.head(5))
#4
print(data1.head(10))
#5
print(data1.columns)
#6
#h = np.array()
h = data1.to_numpy
print(h)

#7
print(data1.describe())

#8
print(data1.sort_values(by=['Product']))

#9
data1.to_csv('test.csv')

#10
path = "/Users/ramiroperez/Downloads/stores.csv"
data2 = pd.read_csv(path)
print(data2)

#11
#data3 = data2[3:4]
#print(data3)

#12 y 13
json_File = {"Product":{"0":"Desktop Computer","1":"Tablet","2":"iPhone","3":"Laptop"},"Price":{"0":700,"1":250,"2":800,"3":1200}}
data3 = pd.DataFrame.from_dict(json_File)
print(data3)

#14
data4 = pd.DataFrame.to_excel('test.xls')

